import QtQuick 2.7
import QtQuick.Controls 2.1
import EraListModel 1.0;


Rectangle {
    id:_awaiting
    color: "lightgrey"
    border.color: "green"
    property var isTesting:false

    Connections{
        target: _listModel
        onListModelReady: _awaiting.color="white"
    }

    Connections{
        target: _listModel
        onItTestingModule: isTesting=true
    }

    Connections{
        target: _listModel
        onNotTestingModule: isTesting=false
    }

    Connections{
        target:_backButton
        onClicked:{
            _listModel.clearList();
            _awaiting.color="lightgrey";
        }
    }

    Connections{
        target: _listModel
        onListViewWindowOpened:{
            _awaiting.color="lightgrey";
            _epochList.listLoading();
        }
    }

    Connections{
        target: _updateWindow
        onUpdateModuleLoadButtonPressed:_listModel.getSelectedElements(false,-1)
    }
    Connections{
        target: _testSettingsWindow
        onTestingModuleStartButtonPressed:{
            _listModel.getSelectedElements(true, _chooseNumberOfQuestions.currentIndex);
            console.log("signal activated");
        }
    }

    ListView {
        id: _listViewId
        anchors.fill: _epochList
        anchors.topMargin: _epochList.width*0.002//чтобы рамка не перекрывалась
        anchors.bottomMargin: _epochList.width*0.004
        anchors.leftMargin: _epochList.width*0.004
        anchors.rightMargin: _epochList.width*0.004
        width:_epochList.width
        height: _epochList.height
        clip: true
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds

        model: EraListModel{
            id:_listModel
        }
        delegate: EpochListItem {
            height: _epochList.height*0.2
            width: _epochList.width
        }
        focus: true
        ScrollBar.vertical: ScrollBar {}
        Connections{
            target: _switchPanel
            onActivated: _listModel.changeListOfTheSelectedEpoch(firstSwitchChecked,secondSwitchChecked)
        }
        //Connections{
        //    target: _loadImagesButton
        //  onClicked: _progressBarWindow.show()//,updater.start(_listModel.getSelectedElements())
        //}
    }

}



