import QtQuick 2.7
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.1


/*Window {
    id: win
    visible: true
    width: 800
    height: 600
    title: qsTr("Hello World")

    Repeater {
        model: 10
        Rectangle {
            id: rect
            width: 50
            height: 50
            z: mouseArea.drag.active ||  mouseArea.pressed ? 2 : 1
            color: Qt.rgba(Math.random(), Math.random(), Math.random(), 1)
            x: Math.random() * (win.width / 2 - 100)
            y: Math.random() * (win.height - 100)
            property point beginDrag
            property bool caught: false
            border { width:2; color: "white" }
            radius: 5
            Drag.active: mouseArea.drag.active

            Text {
                anchors.centerIn: parent
                text: index
                color: "white"
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                drag.target: parent
                onPressed: {
                    //rect.beginDrag = Qt.point(rect.x, rect.y);
                }
                onReleased: {
                    if(!rect.caught || dragTarget.dropped) {
                        backAnimX.from = rect.x;
                        backAnimX.to = beginDrag.x;
                        backAnimY.from = rect.y;
                        backAnimY.to = beginDrag.y;
                        backAnim.start()
                    }

                    parent.Drag.drop()

                    console.log("MouseArea - containsDrag " + dragTarget.dropped)
                }

            }
            ParallelAnimation {
                id: backAnim
                SpringAnimation { id: backAnimX; target: rect; property: "x";
                                  duration: 100; spring: 2; damping: 0.2 }
                SpringAnimation { id: backAnimY; target: rect; property: "y";
                                  duration: 100; spring: 2; damping: 0.2 }
            }
        }
    }

    Rectangle {
        anchors {
            top: parent.top
            right:  parent.right
            bottom:  parent.bottom
        }
        width: parent.width / 2
        color: "gold"
        DropArea {
            id: dragTarget
            anchors.fill: parent

            property bool dropped: false

            onEntered: {
                console.log("onEntered " + containsDrag)
                drag.source.caught = true;
            }
            onExited: {
                console.log("onExited " + containsDrag)
                dropped = false;
            }
            onDropped:
            {
                console.log("onDropped");
                dropped = true;
            }
        }
    }
}*/


Rectangle {
    id:_testWindow
    color:"lemonchiffon"
    Rectangle{
        id:_taskTextContainer
        height: _testWindow.height*0.04
        width: _testWindow.width
        anchors.top: _testWindow.top
        color: _testWindow.color
        Text {
            id: _taskText
            anchors.centerIn: _taskTextContainer
            text: titles.isEraQuestions?titles.getEraTitle():titles.getArtTitle()
            font{
                pointSize:Math.min(_taskTextContainer.height===0?1:_taskTextContainer.height/2
                                   ,_taskTextContainer.width===0?1:_taskTextContainer.width*0.03)
            }
            verticalAlignment: Text.AlignVCenter
        }
    }
    DragGrid{
        id:_dADGrid
        width: (_testWindow.width/2)-_testWindow.width*0.005
        height: _testWindow.height*0.89
        anchors.top: _taskTextContainer.bottom
        anchors.left: parent.left
        //anchors.right: _dropGrid.left
        //anchors.bottom: _nextButton.top
        //anchors.bottomMargin: _nextButton.height*0.03
    }
    DropGrid{
        id:_dropGrid
        width: (_testWindow.width/2)-_testWindow.width*0.005
        height: _testWindow.height*0.89
        //anchors.left: _dADGrid.right
        anchors.right: parent.right
        anchors.top: _taskTextContainer.bottom
        //anchors.bottom: _nextButton.top
        //anchors.bottomMargin: _nextButton.height*0.03
    }
    Item{
        id:_dragContainer
        width: _dropGrid.width*0.45
        height: _dropGrid.height*0.45
    }

    Button {
        id:_nextButton
        anchors.bottom:parent.bottom
        anchors.right: parent.right
        width: _testWindow.width*0.9
        height: _testWindow.height*0.06
        //anchors.topMargin: _testWindow.height*0.07
        anchors.rightMargin: _testWindow.width*0.05
        Text{
            anchors.centerIn: _nextButton
            text: qsTr("Дальше")
            font{
                pointSize: Math.min(_nextButton.height===0?1:_nextButton.height
                                    ,_nextButton.width===0?1:_nextButton.width)/2
            }
        }
        background: Rectangle
        {
            color:_nextButton.down? "lawngreen":"greenyellow"
            radius:_nextButton.height
        }
        //onClicked: buttonsHandler.loadButtonPressed()
    }
}

