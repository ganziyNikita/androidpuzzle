import QtQuick 2.7


Item {
    id:root
    property string typeKey:itemType

    Rectangle{
        id:_imageRectangle
        anchors.fill: parent
        anchors.margins: Math.min(root.height*0.05,root.width*0.05)
        border{
            color: "green"
            width: Math.min(root.height===0?1:root.height*0.005,
                            root.width===0?1:root.width*0.005)
        }
        MouseArea {
            id: mouseArea
            width: _imageRectangle.width; height: _imageRectangle.height
            anchors.centerIn: parent
            property Item dropObject:null
            drag.target: _dragImage
            Image {
                id: _dragImage
                source: "../../"+dragItemImageSource
                property string name:dragItemImageSource
                property bool caught: false
                width: parent.width
                height: parent.height
                antialiasing: true
                fillMode: Image.PreserveAspectCrop
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Drag.keys: [ typeKey]
                Drag.active: mouseArea.drag.active
                Drag.hotSpot.x: 32
                Drag.hotSpot.y: 32
                states: State {
                    when: mouseArea.drag.active
                    ParentChange { target: _dragImage; parent: _dragContainer }
                    AnchorChanges { target: _dragImage; anchors.horizontalCenter: undefined;anchors.verticalCenter: undefined}
                }

            }
            onReleased: {
                //parent = _dragImage.Drag.target !== null ? (_dragImage.Drag.target) : root
                if(dropObject===null)//айтем нигде не был
                {
                    if(_dragImage.Drag.target.objectName==="DropArea")
                    {
                        console.log(_dragImage.Drag.target.isBusy)
                        if(!_dragImage.Drag.target.isBusy)//если перемещаем в дроп и он свободен
                        {
                            dropObject=_dragImage.Drag.target//запомнили, что находимся в конкретном дропе
                            parent=_dragImage.Drag.target//переместили в дроп
                            _dragImage.Drag.drop()//сигнал дропа, теперь ячейка занята
                            dropObject.changeDroppedItemName(dragItemName)

                        }
                        else
                            parent=root//Дроп занят, значит вернулись
                    }
                    else
                        parent=root////Дроп занят, значит вернулись или перемещают не в дроп, то вернуть на место
                }
                else
                {
                    if(_dragImage.Drag.target===null)//если по нему кликнули или потащили назад в его грид
                    {
                        dropObject.isBusy=false
                        dropObject=null
                        parent=root
                    }
                    else //если его перетаскивают в другой дроп
                    {
                        if(!_dragImage.Drag.target.isBusy)//он свободен
                        {
                        dropObject.isBusy=false//текущий освободили
                        _dragImage.Drag.target.isBusy=true//следующий заняли
                        dropObject=_dragImage.Drag.target//запомнили где будем находиться
                        parent=_dragImage.Drag.target
                        dropObject.changeDroppedItemName(dragItemName)
                        }

                    }
                }

                /*if(dropObject!=null && dropObject!=_dragImage.Drag.target.ObjectName)
                    dropObject.isBusy=false
                else
                {
                    dropObject=_dragImage.Drag.target
                    _dragImage.Drag.drop()
                }*/
                /*//if(_dragImage.Drag.target===DropArea)
                      //  console.log("ETODROP")
                    //else
                        //console.log("NEDROP")
                    dropObject=_dragImage.Drag.target
                    console.log(dropObject.objectName)*/

                //_dragImage.Drag.drop()

            }

        }
    }
}



/*Rectangle{
    id:dndDelegate
    color:"lemonchiffon"
    MouseArea{
        id:_mouseArea
        width: dndDelegate.width
        height: dndDelegate.height
        drag.target:_firstItemImage,_secondItemImage,_thirdItemImage,_fourthItemImage

        onReleased: parent = _firstItemImage.Drag.target !== null ? _firstItemImage.Drag.target :_firstHalfOFScreen

        Rectangle{
            id:_firstHalfOFScreen
            width: (dndDelegate.width/2)-dndDelegate.width*0.005
            height: dndDelegate.height

            Image {
                id: _firstItemImage
                source: firstImage
                width: _firstHalfOFScreen.width*0.3
                height: _firstHalfOFScreen.height*0.4
                anchors.top: _firstHalfOFScreen.top
                anchors.topMargin: _firstHalfOFScreen.height*0.04
                anchors.left: _firstHalfOFScreen.left
                anchors.leftMargin: _firstHalfOFScreen.width*0.03
                smooth: true
                fillMode: Image.PreserveAspectFit
                Drag.active: _mouseArea.drag.active
                Drag.hotSpot.x: 32
                Drag.hotSpot.y: 32
                states: State {
                when: mouseArea.drag.active
                ParentChange { target: _firstItemImage; parent: _firstHalfOFScreen }
                AnchorChanges { target: _firstItemImage; anchors.verticalCenter: undefined; anchors.horizontalCenter: undefined }
                }
            }

            Image {
                id: _secondItemImage
                source: secondImage
                width: _firstHalfOFScreen.width*0.3
                height: _firstHalfOFScreen.height*0.4
                anchors.top: _firstHalfOFScreen.top
                anchors.topMargin: _firstHalfOFScreen.height*0.04
                anchors.right: _firstHalfOFScreen.right
                anchors.rightMargin: _firstHalfOFScreen.width*0.03
                smooth: true
                fillMode: Image.PreserveAspectFit
            }

            Image {
                id: _thirdItemImage
                source: thirdImage
                width: _firstHalfOFScreen.width*0.3
                height: _firstHalfOFScreen.height*0.4
                anchors.top: _firstItemImage.bottom
                anchors.topMargin: _firstHalfOFScreen.height*0.1
                anchors.left: _firstHalfOFScreen.left
                anchors.leftMargin: _firstHalfOFScreen.width*0.03
                smooth: true
                fillMode: Image.PreserveAspectFit
            }

            Image {
                id: _fourthItemImage
                source: fourthImage
                width: _firstHalfOFScreen.width*0.3
                height: _firstHalfOFScreen.height*0.4
                anchors.top: _secondItemImage.bottom
                anchors.topMargin: _firstHalfOFScreen.height*0.1
                anchors.right: _firstHalfOFScreen.right
                anchors.rightMargin: _firstHalfOFScreen.width*0.03
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

    }


    /*Rectangle{
        id:_secondHalfOfScreen
        width: (dndDelegate.width/2)-dndDelegate.width*0.005
        height: dndDelegate.height
        anchors.left: _firstHalfOFScreen.right
        anchors.leftMargin: dndDelegate.width*0.01
        Rectangle{
            id:_fifthPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _secondHalfOfScreen.top
            anchors.topMargin: _secondHalfOfScreen.height*0.04
            anchors.left: _secondHalfOfScreen.left
            anchors.leftMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _fifthItemImage
                source: fifthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_sixthPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _secondHalfOfScreen.top
            anchors.topMargin: _secondHalfOfScreen.height*0.04
            anchors.right: _secondHalfOfScreen.right
            anchors.rightMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _sixthItemImage
                source: sixthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_seventhPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _fifthPicture.bottom
            anchors.topMargin: _secondHalfOfScreen.height*0.1
            anchors.left: _secondHalfOfScreen.left
            anchors.leftMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _seventhItemImage
                source: seventhImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_eighthPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _sixthPicture.bottom
            anchors.topMargin: _secondHalfOfScreen.height*0.1
            anchors.right: _secondHalfOfScreen.right
            anchors.rightMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _eighthItemImage
                source: eighthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

    }
    /*states: [
        State {
            name: "inDrag"
            when: index == dndGrid.draggedItemIndex
            PropertyChanges { target: _fourthPicture; parent: dndContainer }
            PropertyChanges { target: _fourthPicture; anchors.top: undefined }
            PropertyChanges { target: _fourthPicture; anchors.topMargin: undefined }
            PropertyChanges { target: _fourthPicture; anchors.leftMargin: undefined }
            PropertyChanges { target: _fourthPicture; anchors.left: undefined }
            PropertyChanges { target: _fourthPicture; x: coords.mouseX - _fourthPicture.width / 2 }
            PropertyChanges { target: _fourthPicture; y: coords.mouseY - _fourthPicture.height / 2 }
        }
    ]*/

//}




/*Rectangle{
    id:dndDelegate
    color:"lemonchiffon"
    Rectangle{
        id:_firstHalfOFScreen
        width: (dndDelegate.width/2)-dndDelegate.width*0.005
        height: dndDelegate.height

        Item{
            id:_firstPicture
            width: _firstHalfOFScreen.width*0.3
            height: _firstHalfOFScreen.height*0.4
            anchors.top: _firstHalfOFScreen.top
            anchors.topMargin: _firstHalfOFScreen.height*0.04
            anchors.left: _firstHalfOFScreen.left
            anchors.leftMargin: _firstHalfOFScreen.width*0.03
            Image {
                id: _firstItemImage
                source: firstImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_secondPicture
            width: _firstHalfOFScreen.width*0.3
            height: _firstHalfOFScreen.height*0.4
            anchors.top: _firstHalfOFScreen.top
            anchors.topMargin: _firstHalfOFScreen.height*0.04
            anchors.right: _firstHalfOFScreen.right
            anchors.rightMargin: _firstHalfOFScreen.width*0.03
            Image {
                id: _secondItemImage
                source: secondImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_thirdPicture
            width: _firstHalfOFScreen.width*0.3
            height: _firstHalfOFScreen.height*0.4
            anchors.top: _firstPicture.bottom
            anchors.topMargin: _firstHalfOFScreen.height*0.1
            anchors.left: _firstHalfOFScreen.left
            anchors.leftMargin: _firstHalfOFScreen.width*0.03
            Image {
                id: _thirdItemImage
                source: thirdImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_fourthPicture
            width: _firstHalfOFScreen.width*0.3
            height: _firstHalfOFScreen.height*0.4
            anchors.top: _secondPicture.bottom
            anchors.topMargin: _firstHalfOFScreen.height*0.1
            anchors.right: _firstHalfOFScreen.right
            anchors.rightMargin: _firstHalfOFScreen.width*0.03
            Image {
                id: _fourthItemImage
                source: fourthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

    }


    Rectangle{
        id:_secondHalfOfScreen
        width: (dndDelegate.width/2)-dndDelegate.width*0.005
        height: dndDelegate.height
        anchors.left: _firstHalfOFScreen.right
        anchors.leftMargin: dndDelegate.width*0.01
        Rectangle{
            id:_fifthPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _secondHalfOfScreen.top
            anchors.topMargin: _secondHalfOfScreen.height*0.04
            anchors.left: _secondHalfOfScreen.left
            anchors.leftMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _fifthItemImage
                source: fifthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_sixthPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _secondHalfOfScreen.top
            anchors.topMargin: _secondHalfOfScreen.height*0.04
            anchors.right: _secondHalfOfScreen.right
            anchors.rightMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _sixthItemImage
                source: sixthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_seventhPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _fifthPicture.bottom
            anchors.topMargin: _secondHalfOfScreen.height*0.1
            anchors.left: _secondHalfOfScreen.left
            anchors.leftMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _seventhItemImage
                source: seventhImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

        Item{
            id:_eighthPicture
            width: _secondHalfOfScreen.width*0.3
            height: _secondHalfOfScreen.height*0.4
            anchors.top: _sixthPicture.bottom
            anchors.topMargin: _secondHalfOfScreen.height*0.1
            anchors.right: _secondHalfOfScreen.right
            anchors.rightMargin: _secondHalfOfScreen.width*0.03
            Image {
                id: _eighthItemImage
                source: eighthImage
                anchors.centerIn: parent
                anchors.fill: parent
                smooth: true
                fillMode: Image.PreserveAspectFit
            }
        }

    }
    states: [
        State {
            name: "inDrag"
            when: index == dndGrid.draggedItemIndex
            PropertyChanges { target: _fourthPicture; parent: dndContainer }
            PropertyChanges { target: _fourthPicture; anchors.top: undefined }
            PropertyChanges { target: _fourthPicture; anchors.topMargin: undefined }
            PropertyChanges { target: _fourthPicture; anchors.leftMargin: undefined }
            PropertyChanges { target: _fourthPicture; anchors.left: undefined }
            PropertyChanges { target: _fourthPicture; x: coords.mouseX - _fourthPicture.width / 2 }
            PropertyChanges { target: _fourthPicture; y: coords.mouseY - _fourthPicture.height / 2 }
        }
    ]

}*/

/*
import QtQuick 2.7


Rectangle{

    Component{
        id:dndDelegate
        Item {
            id: wrapper
            width: dndDelegate.cellWidth
            height: dndDelegate.cellHeight
            Rectangle{
                id:_firstHalfOFScreen
                width: (dndDelegate.width/2)-dndDelegate.width*0.005
                height: dndDelegate.height

                Rectangle{
                    id:_firstPicture
                    width: _firstHalfOFScreen.width*0.3
                    height: _firstHalfOFScreen.height*0.4
                    anchors.top: _firstHalfOFScreen.top
                    anchors.topMargin: _firstHalfOFScreen.height*0.04
                    anchors.left: _firstHalfOFScreen.left
                    anchors.leftMargin: _firstHalfOFScreen.width*0.03
                    Image {
                        id: _firstItemImage
                        source: firstImage
                        anchors.centerIn: parent
                        width: _firstPicture.width
                        height: _firstPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    id:_secondPicture
                    width: _firstHalfOFScreen.width*0.3
                    height: _firstHalfOFScreen.height*0.4
                    anchors.top: _firstHalfOFScreen.top
                    anchors.topMargin: _firstHalfOFScreen.height*0.04
                    anchors.right: _firstHalfOFScreen.right
                    anchors.rightMargin: _firstHalfOFScreen.width*0.03
                    Image {
                        id: _secondItemImage
                        source: secondImage
                        anchors.centerIn: parent
                        width: _secondPicture.width
                        height: _secondPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    id:_thirdPicture
                    width: _firstHalfOFScreen.width*0.3
                    height: _firstHalfOFScreen.height*0.4
                    anchors.top: _firstPicture.bottom
                    anchors.topMargin: _firstHalfOFScreen.height*0.1
                    anchors.left: _firstHalfOFScreen.left
                    anchors.leftMargin: _firstHalfOFScreen.width*0.03
                    Image {
                        id: _thirdItemImage
                        source: thirdImage
                        anchors.centerIn: parent
                        width: _thirdPicture.width
                        height: _thirdPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    id:_fourthPicture
                    width: _firstHalfOFScreen.width*0.3
                    height: _firstHalfOFScreen.height*0.4
                    anchors.top: _secondPicture.bottom
                    anchors.topMargin: _firstHalfOFScreen.height*0.1
                    anchors.right: _firstHalfOFScreen.right
                    anchors.rightMargin: _firstHalfOFScreen.width*0.03
                    Image {
                        id: _fourthItemImage
                        source: fourthImage
                        anchors.centerIn: parent
                        width: _thirdPicture.width
                        height: _thirdPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

            }


            Rectangle{
                id:_secondHalfOfScreen
                width: (dndDelegate.width/2)-dndDelegate.width*0.005
                height: dndDelegate.height
                anchors.left: _firstHalfOFScreen.right
                anchors.leftMargin: dndDelegate.width*0.01
                Rectangle{
                    id:_fifthPicture
                    width: _secondHalfOfScreen.width*0.3
                    height: _secondHalfOfScreen.height*0.4
                    anchors.top: _secondHalfOfScreen.top
                    anchors.topMargin: _secondHalfOfScreen.height*0.04
                    anchors.left: _secondHalfOfScreen.left
                    anchors.leftMargin: _secondHalfOfScreen.width*0.03
                    Image {
                        id: _fifthItemImage
                        source: fifthImage
                        anchors.centerIn: parent
                        width: _fifthPicture.width
                        height: _fifthPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    id:_sixthPicture
                    width: _secondHalfOfScreen.width*0.3
                    height: _secondHalfOfScreen.height*0.4
                    anchors.top: _secondHalfOfScreen.top
                    anchors.topMargin: _secondHalfOfScreen.height*0.04
                    anchors.right: _secondHalfOfScreen.right
                    anchors.rightMargin: _secondHalfOfScreen.width*0.03
                    Image {
                        id: _sixthItemImage
                        source: sixthImage
                        anchors.centerIn: parent
                        width: _sixthPicture.width
                        height: _sixthPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    id:_seventhPicture
                    width: _secondHalfOfScreen.width*0.3
                    height: _secondHalfOfScreen.height*0.4
                    anchors.top: _fifthPicture.bottom
                    anchors.topMargin: _secondHalfOfScreen.height*0.1
                    anchors.left: _secondHalfOfScreen.left
                    anchors.leftMargin: _secondHalfOfScreen.width*0.03
                    Image {
                        id: _seventhItemImage
                        source: seventhImage
                        anchors.centerIn: parent
                        width: _seventhPicture.width
                        height: _seventhPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

                Rectangle{
                    id:_eighthPicture
                    width: _secondHalfOfScreen.width*0.3
                    height: _secondHalfOfScreen.height*0.4
                    anchors.top: _sixthPicture.bottom
                    anchors.topMargin: _secondHalfOfScreen.height*0.1
                    anchors.right: _secondHalfOfScreen.right
                    anchors.rightMargin: _secondHalfOfScreen.width*0.03
                    Image {
                        id: _eighthItemImage
                        source: eighthImage
                        anchors.centerIn: parent
                        width: _eighthPicture.width
                        height: _eighthPicture.height
                        smooth: true
                        fillMode: Image.PreserveAspectFit
                    }
                }

            }
            states: [
                State {
                    name: "inDrag"
                    when: index == dndGrid.draggedItemIndex
                    PropertyChanges { target: _firstPicture; parent: dndContainer }
                    PropertyChanges { target: _firstPicture; anchors.top: undefined }
                    PropertyChanges { target: _firstPicture; anchors.left: undefined }
                    PropertyChanges { target: _firstPicture; x: coords.mouseX - itemImage.width / 2 }
                    PropertyChanges { target: _firstPicture; y: coords.mouseY - itemImage.height / 2 }
                }
            ]

        }
    }
}



  */
