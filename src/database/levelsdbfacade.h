#ifndef LEVELSDBFACADE_H
# define LEVELSDBFACADE_H

#include "dbfacade.h"
#include <QSqlRelationalTableModel>
#include <map>
#include "singleton.h"
#include "levelstructures.h"
#include "settingsdbfacade.h"
#include "jsondocument.h"
#include <vector>

class LevelsDBFacade : public DBFacade {
  Q_OBJECT
public:
  LevelsDBFacade(QObject *parent = nullptr);

  Art randomArt();
  Art randomArt(Era& era);
  Art randomArt(Author& author);

  std::vector<Author> authorsByNameArt(const QString& artName);
  QSet<Author> getAuthorsByNameArt(const QString& artName);
  std::vector<Author> getAuthorsOfArt(const QString& artName);

  std::vector<Author> authors();
  std::vector<Era> eras();
  QSet<Era> getEras();
  QSet<Art> getArts();
  Author getAuthorForCompare(QSet<Author>::iterator authorFromLoadedFile);


  std::vector<Art> arts();
  std::vector<Art> arts(Era& era);
  QSet<Art> getArts(const Era& era);
  std::vector<Art> arts(Author& author);

  void loadLevels(const QByteArray &jsonData);

  ///for update
  void updateEra(const Era &era);
  ///for update
  void updateArt(Art &art);
  ///for update
  void updateAuthor(Author &author);

  void addArt(const Art& Art);
  void addEra(const Era& era);
  void addAuthor(const Author& author);
  void addArtAuthor(const QString authorName, const QString artName);

protected:
  QSqlRelationalTableModel *m_arts_model;
  QSqlRelationalTableModel *m_eras_model;
  QSqlRelationalTableModel *m_authors_model;
  QSqlRelationalTableModel *m_artsAuthors_model;

  friend class Singleton<LevelsDBFacade>;
};

#define DB Singleton<LevelsDBFacade>::instance()

#endif // LEVELSDBFACADE_H
