#ifndef JSONDOCUMENT_H
#define JSONDOCUMENT_H

#include <QByteArray>
#include <QJsonDocument>
#include <QDate>
#include <QObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <database/levelstructures.h>
#include "settings/update/pixmaploader.h"
#include <QDate>
#include <QSet>

class JsonDocument:public QObject
{
  Q_OBJECT
public:
  JsonDocument();

  QSet<Era> &getEras();
  QSet<Art> getArts();
  QSet<Art> getArts(const Era &era);
  QSet<Author> getAuthors();

  void prepareErasArray();
  void prepareArtsArray();

  void readJson(const QByteArray &jsonData);


  Author getAuthor(QString authorName);
  //<For update
  QSet<Author> getArtAuthors(const Art &art);
  Author &getAuthorByName(QString &authorName);
  //For update>

private:

  QJsonDocument jsonDoc;

  QSet<Era> m_erasArray;
  QSet<Art> m_artsArray;
  QSet<Author> m_authorsArray;

  std::multimap<Art,Author> m_artAuthors;

};

#endif // JSONDOCUMENT_H
