#ifndef LEVELSTRUCTURES_H
#define LEVELSTRUCTURES_H
#include <QString>
#include <QVariant>
#include <QDate>


struct Era {
  mutable QString name, imgPath; QDate lastUpdate;

  Era(QString name_, QString imgPath_, QDate lastUpdate_)
    : name(name_), imgPath(imgPath_), lastUpdate(lastUpdate_) {
  }

  friend uint qHash(const Era &era){
      return qHash(era.name);
  }
  bool operator==(const Era &other) const
  {
    return other.name==name;
  }

};

struct Art {

  QString eraName, imgPath, imgInfo, imgName;bool domestic; QDate lastUpdate;

  Art(QString eraName_, QString imgPath_,  QString imgInfo_, QString name_, bool domestic_, QDate lastUpdate_)
    : eraName(eraName_), imgPath(imgPath_), imgInfo(imgInfo_), imgName(name_), domestic(domestic_), lastUpdate(lastUpdate_){
  }

  friend uint qHash(const Art &art){
      return qHash(art.imgName);
  }

  //find??
  bool operator==(const Art &other) const{
    return other.eraName==eraName;
  }

  friend bool operator<(const Art& x, const Art& y)
  {
  return x.imgName>y.imgName;
  }
};


struct Author {
  QString authorName, imgPath, authorInfo; QDate lastUpdate;

  Author(QString authorName_, QString imgPath_, QString authorInfo_, QDate lastUpdate_)
    : authorName(authorName_), imgPath(imgPath_), authorInfo(authorInfo_), lastUpdate(lastUpdate_){
  }

  ///for qset
  friend uint qHash(const Author &author){
      return qHash(author.authorName);
  }
  ///for find
  bool operator==(const Author &other) const{
    return other.authorName==authorName;
  }
  ///for qmultimap
  friend bool operator<(const Author& x, const Author& y)
  {
  return x.authorName>y.authorName;
  }
};

struct EraListModelItem{
  QString eraName;
  int domesticFilesCount;
  int internationalFilesCount;
  bool eraNeedToUpdate;
  bool domesticSelected;
  bool internationalSelected;
  bool checkValue;

  EraListModelItem(QString eraName_,int domesticFilesCount_, int internationalFilesCount_,
                   bool eraNeedToUpdate_, bool checkValue_, bool domesticSelected_, bool internationalSelected_):
    eraName(eraName_), domesticFilesCount(domesticFilesCount_), internationalFilesCount(internationalFilesCount_),
    eraNeedToUpdate(eraNeedToUpdate_), domesticSelected(domesticSelected_), internationalSelected(internationalSelected_), checkValue(checkValue_){
  }
  EraListModelItem& operator=(const QVariant newValue){
    this->checkValue=newValue.toBool();
    return *this;
  }
  bool operator==(const QString &findEraName) const{
    return eraName==findEraName;
  }
};

struct DropGridItem{
  QString itemType;
  QString dropItemName;
  QString dropItemImageSource;
  QString answerObjectName;
  //Возможно поле parent, если нужна реализация возврата в тестах

  DropGridItem(QString itemType_, QString dropItemName_, QString dropItemImageSource_, QString answerObjectName_):
    itemType(itemType_),dropItemName(dropItemName_), dropItemImageSource(dropItemImageSource_), answerObjectName(answerObjectName_){}

  DropGridItem& operator=(const QString newValue){
    this->answerObjectName=newValue;
    return *this;
  }
};

struct DragGridItem{
  QString itemType;
  QString dragItemName;
  QString dragItemImageSource;
  int answerIndex;
  //Возможно поле parent, если нужна реализация возврата в тестах

  DragGridItem(QString itemType_, QString dragItemName_, QString dragItemImageSource_, int answerIndex_):
    itemType(itemType_),dragItemName(dragItemName_), dragItemImageSource(dragItemImageSource_), answerIndex(answerIndex_){}
  DragGridItem& operator=(const size_t newValue){
    this->answerIndex=newValue;
    return *this;
  }
};

#endif // LEVELSTRUCTURES_H
