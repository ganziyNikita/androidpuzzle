#include "jsondocument.h"
#include <vector>
#include <set>

JsonDocument::JsonDocument()
{
}

QSet<Era> &JsonDocument::getEras()
{
  return m_erasArray;
}

QSet<Art> JsonDocument::getArts()
{
 return m_artsArray;
}

QSet<Art> JsonDocument::getArts(const Era &era)
{
  Art tmp(era.name, "", "", "",false, QDate());

  QSet<Art> arts;

  auto it=m_artsArray.begin();
  //auto it = m_artAuthors.find(tmp);
  //auto it2=m_artAuthors.equal_range(tmp);

  while(it!=m_artsArray.end())
  {
    if(it->eraName==era.name)
    arts.insert(*it);
    ++it;
  }
  ///Можно equal_range, но медленее
  /// Сперва проверка на конец списка, потом на совпадение, иначе исключение
  /*while (it!=m_artAuthors.end() && it->first.eraName == era.name) {
    arts.insert(it->first);
    ++it;
  }*/

  return arts;
}

QSet<Author> JsonDocument::getAuthors()
{
  QSet<Author> authors;

  for (auto pair : m_artAuthors)
    authors.insert(pair.second);
  return authors;
}

void JsonDocument::prepareErasArray()
{
  QJsonArray erasArray;
  erasArray=QJsonValue(jsonDoc.object().value("eras")).toArray();
  for(int index=0;index<erasArray.size();index++)
  {
    Era era(erasArray.at(index).toObject().value("eraName").toString(),
            erasArray.at(index).toObject().value("eraImagePath").toString(),
            QDate::fromString(erasArray.at(index).toObject().value("lastUpdate").toString(), "yyyy-MM-dd"));
    m_erasArray.insert(era);
  }
}

void JsonDocument::prepareArtsArray()
{
  QJsonArray artsArray;
  artsArray=QJsonValue(jsonDoc.object().value("arts")).toArray();
  for(int index=0;index<artsArray.size();index++)
  {
    Art art(artsArray.at(index).toObject().value("artEra").toString(),
            artsArray.at(index).toObject().value("artPath").toString(),
            artsArray.at(index).toObject().value("artInfo").toString(),
            artsArray.at(index).toObject().value("artName").toString(),
            artsArray.at(index).toObject().value("isDomestic").toBool(),
            QDate::fromString(artsArray.at(index).toObject().value("lastUpdate").toString(), "yyyy-MM-dd"));
    QJsonArray m_artAuthorsLoc=artsArray.at(index).toObject().value("artAuthors").toArray();


    m_artsArray.insert(art);
    ///MultiMap<Art,Author> заполнение
    for(int i=0;i<m_artAuthorsLoc.size();i++)
    {
      Author author=getAuthor(m_artAuthorsLoc[i].toString());
      m_artAuthors.insert(std::make_pair(art,author));
    }
  }
}


void JsonDocument::readJson(const QByteArray &jsonData)
{
  QJsonParseError docError;
  jsonDoc = QJsonDocument::fromJson(jsonData, &docError);
  if (docError.error != QJsonParseError::NoError) {
    throw docError.errorString();
  }
  prepareErasArray();
  prepareArtsArray();
}

Author JsonDocument::getAuthor(QString authorName)
{
  //exception
  QJsonArray authorsArray;
  authorsArray=QJsonValue(jsonDoc.object().value("authors")).toArray();
  for(int index=0;index<authorsArray.size();index++)
  {
    if(authorsArray.at(index).toObject().value("authorName").toString()==authorName)
    {
      Author author(authorsArray.at(index).toObject().value("authorName").toString(),
                    authorsArray.at(index).toObject().value("authorImagePath").toString(),
                    authorsArray.at(index).toObject().value("authorInfo").toString(),
                    QDate::fromString(authorsArray.at(index).toObject().value("lastUpdate").toString(), "yyyy-MM-dd"));
      return author;
    }
  }
}

QSet<Author> JsonDocument::getArtAuthors(const Art &art)
{
  QSet<Author> artAuthors;

  std::pair <std::multimap<Art,Author>::iterator, std::multimap<Art,Author>::iterator> ret;
  ret=m_artAuthors.equal_range(art);

  ///Обход всех авторов переданной картины
  for(std::multimap<Art,Author>::iterator itr=ret.first;itr!=ret.second;++itr)
  {
    artAuthors.insert(itr->second);
  }

  return artAuthors;
}


