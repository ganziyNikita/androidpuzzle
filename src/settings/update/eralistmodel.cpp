#include "eralistmodel.h"

#include "eralistmodel.h"
#include "indexloader.h"
#include "updater.h"
#include "testing/testmanager.h"
#include <QDebug>

EraListModel::EraListModel(QObject *parent)
  :QAbstractListModel(parent)
{
  clearList();
  connect(&UPDATER, SIGNAL(itemsLoaded(std::vector<EraListModelItem>, bool)), this,
          SLOT(fillEras(std::vector<EraListModelItem>, bool)));
  connect(&TESTMANAGER, SIGNAL(erasReady(std::vector<EraListModelItem>, bool)), this,
          SLOT(fillEras(std::vector<EraListModelItem>, bool)));
}

void EraListModel::fillEras(std::vector<EraListModelItem> vec, bool isTestingModule)
{
  clearList();
  //cигнал, уведомляющий о том, что лист вью используются в модуле тестирования
  // и количество файлов выводить нет необходимости
  if(isTestingModule)
    emit itTestingModule();
  else
    emit notTestingModule();

  emit listViewWindowOpened();
  //removeRows(0,m_eraListModel.size(),QModelIndex());

  ///Для передачи информации в listView о изменении модели необходимо
  /// Вызвать метод beginInsertRows, уведомляющий об измении модели на заданное кол-во элементов
  /// изменить модель
  /// вызвать метод, уведомляющий о завершении изменения модели
  //beginInsertRows(QModelIndex(),0,vec.size()-1);
  //m_eraListModel=vec;
  allErasForLoading=vec;
  qDebug()<<"FILLERAS+"+QString::number(vec.size());
  //endInsertRows();
  emit listModelReady();
}

QString EraListModel::BoolToString(bool b) const
{
  return b ? "true" : "false";
}

int EraListModel::rowCount(const QModelIndex &parent) const
{
  return m_eraListModel.size();
}

bool EraListModel::isPositionValid(int rowIndex) const
{
  return rowIndex<m_eraListModel.size();
}

void EraListModel::getSelectedElements(bool isTestingRequest,int buttonNumber)
{
  std::vector<EraListModelItem> selectedEras;
  selectedEras.clear();
  int itemsCount=0;
  for(std::vector<EraListModelItem>::iterator it = m_eraListModel.begin();it!=m_eraListModel.end();++it)
  {
    if(it->checkValue)
    {
      if(domesticArtSwitchButton)
      {
        it->domesticSelected=true;
        itemsCount+=it->domesticFilesCount;
      }
      if(internationalArtSwitchButton)
      {
        it->internationalSelected=true;
        itemsCount+=it->internationalFilesCount;
      }
      if(it->eraNeedToUpdate)
        itemsCount++;
      selectedEras.push_back(*it);
    }
  }
  if(isTestingRequest)
    TESTMANAGER.startTesting(selectedEras, buttonNumber);
  else
    UPDATER.UploadSelectedItems(selectedEras,itemsCount);
}

void EraListModel::changeListOfTheSelectedEpoch(bool domesticArt, bool foreigntArt)
{
  domesticArtSwitchButton=domesticArt;
  internationalArtSwitchButton=foreigntArt;

  m_fillingList.clear();
  removeRows(0,m_eraListModel.size(),QModelIndex());//если удаление всех строк удачно
  //сделать один проход и поставить проверку на принадлежность к искусству

  for(size_t index=0;index<allErasForLoading.size();index++)
  {
    if(domesticArtSwitchButton && internationalArtSwitchButton && (allErasForLoading[index].domesticFilesCount>0
                                                                   || allErasForLoading[index].internationalFilesCount>0))
    {
      EraListModelItem newItem(allErasForLoading[index]);
      m_fillingList.push_back(newItem);
    }
    else if(domesticArtSwitchButton && allErasForLoading[index].domesticFilesCount>0)
    {
      EraListModelItem newItem(allErasForLoading[index]);
      m_fillingList.push_back(newItem);
    }
    else if(internationalArtSwitchButton &&  allErasForLoading[index].internationalFilesCount>0)
    {
      EraListModelItem newItem(allErasForLoading[index]);
      m_fillingList.push_back(newItem);
    }
  }
  insertRows(0,m_fillingList.size(),QModelIndex());///Что выводить в случае, если не вставил
}

void EraListModel::clearList()
{
  removeRows(0,m_eraListModel.size(),QModelIndex());
  m_eraListModel.clear();
  m_fillingList.clear();
}

QHash<int, QByteArray> EraListModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[NameRole] = "eraName";
  roles[CountRole] = "filesCount";
  roles[CheckRole] = "checkValue";
  return roles;
}

QVariant EraListModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid() || (role!=NameRole && role!=CheckRole && role!=CountRole))
    return QVariant {};
  int rowIndex=index.row();
  if(role==CountRole)
  {
    int addEraToNumber=0;
    if(m_eraListModel[rowIndex].eraNeedToUpdate)
      addEraToNumber=1;
    //эпоха уже оказалась в списке для отображения
    //здесь лишь запрашивается информация о ней
    if(domesticArtSwitchButton && internationalArtSwitchButton)
    {
      if(m_eraListModel[rowIndex].eraNeedToUpdate)
        return QVariant::fromValue(QString::number(m_eraListModel[rowIndex].domesticFilesCount
                                                   +m_eraListModel[rowIndex].internationalFilesCount+addEraToNumber));
      else
        return QVariant::fromValue(QString::number(m_eraListModel[rowIndex].domesticFilesCount
                                                   +m_eraListModel[rowIndex].internationalFilesCount));
    }
    else if(m_eraListModel[rowIndex].domesticFilesCount>0)
      return QVariant::fromValue(QString::number(m_eraListModel[rowIndex].domesticFilesCount+addEraToNumber));
    else
      return QVariant::fromValue(QString::number(m_eraListModel[rowIndex].internationalFilesCount+addEraToNumber));
  }
  if(role==CheckRole)
    return QVariant::fromValue(BoolToString(m_eraListModel[rowIndex].checkValue));
  if(!isPositionValid(rowIndex))
    return QVariant {};
  return QVariant::fromValue(m_eraListModel[rowIndex].eraName);
}

bool EraListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  Q_UNUSED(value)
  /*
   * Cannot assign to return value because function 'operator[]' returns a const value
   * m_EraListModel[rowIndex].checkValue=false;

   * */
  if (index.isValid() && role == CheckRole) {
    int rowIndex=index.row();
    if(!isPositionValid(rowIndex))
      return false;
    m_eraListModel[rowIndex].checkValue=!m_eraListModel[rowIndex].checkValue;
    emit dataChanged(index,index,{CheckRole});
    return true;
  }
  return false;
}

bool EraListModel::removeRows(int row, int count, const QModelIndex &parent)
{
  Q_UNUSED(parent)
  if(count>0)
  {
    beginRemoveRows(QModelIndex(), row, count);
    m_eraListModel.erase(m_eraListModel.begin(),m_eraListModel.end());
    endRemoveRows();
  }
  return true;
}

bool EraListModel::insertRows(int row, int count, const QModelIndex &parent2)
{
  Q_UNUSED(parent2)
  /// Сюда вызов функции, которая запишет во временный вектор нужные значения
  /// После чего в beginInsertRows нужно передать количество созданных элементов
  /// следом в вектор-модель записать значения временного вектора
  /// подать вызвать функцию endInsertRows
  if(count>0){
    beginInsertRows(QModelIndex(),0,count-1);
    m_eraListModel=m_fillingList;
    m_fillingList.clear();
    endInsertRows();
  }
  return true;
}



