#include "updater.h"
#include "QDate"
#include <set>
#include "progressBarWidget.h"
#include <settings/update/indexloader.h>

Updater::Updater()
{
  connect(&LOADER, SIGNAL(loaded(const QByteArray &)), this, SLOT(getUpdatableItems(const QByteArray&)), Qt::AutoConnection);
}

void Updater::getUpdatableItems(const QByteArray &jsonData)
{
  std::vector<EraListModelItem> updatableItems;
  //Получили список всех эпох.
  JsonDocument loadedJson;
  loadedJson.readJson(jsonData);
  getErasForUpdateOrLoad(loadedJson);

  ///Просмотр внутренностей(картины+авторы) каждой эпохи
  ///Добавление в список эпох для загрузки в случае необходимости
  QSet<Updater::EraWrapper>::const_iterator erasIt=erasModules.begin();
  for(;erasIt!=erasModules.end();++erasIt)
  {
    int domesticArtsCount=0;
    int internationalArtsCount=0;

    QSet<Updater::ArtWrapper>::const_iterator domesticArtsBeginIt=erasIt->domesticArtsOfEra.begin();
    QSet<Updater::ArtWrapper>::const_iterator internationalArtsBeginIt=erasIt->internationalArtsOfEra.begin();

    QSet<Updater::ArtWrapper>::const_iterator domesticArtsEndIt=erasIt->domesticArtsOfEra.end();
    QSet<Updater::ArtWrapper>::const_iterator internationalArtsEndIt=erasIt->internationalArtsOfEra.end();

    domesticArtsCount=getItemsCountInsideEra(domesticArtsBeginIt,domesticArtsEndIt);
    internationalArtsCount=getItemsCountInsideEra(internationalArtsBeginIt,internationalArtsEndIt);

    ///Либо есть (авторы/картины) для закачки, либо данную эпоху надо обновить/загрузить
    if(domesticArtsCount>0 || internationalArtsCount>0 || erasIt->updateOrLoad.needtoLoad || erasIt->updateOrLoad.needtoUpdate)
    {
      bool eraNeedToUpdate=false;
      if(erasIt->updateOrLoad.needtoLoad || erasIt->updateOrLoad.needtoUpdate)
        eraNeedToUpdate=true;

      EraListModelItem updatableItem(erasIt->era.name,domesticArtsCount,internationalArtsCount,eraNeedToUpdate,
                                     false, false, false);

      qDebug()<<"ERA NAME"+erasIt->era.name;
      qDebug()<<"DOMESTIC COUNT UPDATER="+QString::number(domesticArtsCount);
      qDebug()<<"INTERNATIONAL COUNT UPDATER="+QString::number(internationalArtsCount);
      updatableItems.push_back(updatableItem);
    }
  }
  emit itemsLoaded(updatableItems,false);
}
//Проверка типа эпохи сделать

void Updater::UploadSelectedItems(std::vector<EraListModelItem> &selectedEras,int itemsCount)
{
  //Количество файлов на загрузку в прогресс бар
  //PROGRESSBAR.setMaxValue(itemsCount);
  emit maxValueCalculated(itemsCount);
  QSet<EraWrapper>::iterator it=erasModules.begin();

  for(;it!=erasModules.end();++it)
  {
    //Если эра присутствует в списке на закачку
    std::vector<EraListModelItem>::iterator foundElemIt=std::find(selectedEras.begin(), selectedEras.end(), it->era.name);
    if(foundElemIt != selectedEras.end())
    {
      EraWrapper downloadableEra=*it;
      downloadEraModule(*it,foundElemIt->domesticSelected,foundElemIt->internationalSelected);
      if(downloadableEra.domesticArtsOfEra.size()==0 & downloadableEra.internationalArtsOfEra.size()==0)
      {
        qDebug()<<"REMOVERAZ";
        erasModules.remove(*it);
      }
    }
  }
}

void Updater::downloadEraModule(const Updater::EraWrapper &eraWrapper,bool updateDomesticFiles, bool updateInternationalFiles)
{
  //Если данные об эпохе тоже должны быть обновлены
  if(eraWrapper.updateOrLoad.needtoLoad || eraWrapper.updateOrLoad.needtoUpdate)
  {
    downloadEra(eraWrapper.era,eraWrapper.updateOrLoad.needtoLoad);
    eraWrapper.updateOrLoad.needtoLoad=false;
    eraWrapper.updateOrLoad.needtoUpdate=false;
  }

  if(updateDomesticFiles)
  {
    QSet<ArtWrapper>::iterator eraDomesticArtsIt=eraWrapper.domesticArtsOfEra.begin();
    for(;eraDomesticArtsIt!=eraWrapper.domesticArtsOfEra.end();++eraDomesticArtsIt)
    {
      ArtWrapper downloadableArtWrapper=*eraDomesticArtsIt;
      downloadArtModule(downloadableArtWrapper);
    }
    eraWrapper.domesticArtsOfEra.clear();
  }

  if(updateInternationalFiles)
  {
    QSet<ArtWrapper>::iterator eraInternationalArtsIt=eraWrapper.internationalArtsOfEra.begin();
    for(;eraInternationalArtsIt!=eraWrapper.internationalArtsOfEra.end();++eraInternationalArtsIt)
    {
      ArtWrapper downloadableArtWrapper=*eraInternationalArtsIt;
      downloadArtModule(downloadableArtWrapper);
    }
    eraWrapper.internationalArtsOfEra.clear();
  }
}

void Updater::downloadEra(const Era &era,bool needToInsert)
{
  LOADERPIXMAP.load(era.imgPath);
  era.imgPath=LOADERPIXMAP.getPicPath();
  if(needToInsert)
    DB.addEra(era);
  else
    DB.updateEra(era);
  emit fileLoaded();
}

void Updater::downloadArtModule(Updater::ArtWrapper &artWrapper)
{
  //Проверка на необходимость обновления картины
  if(artWrapper.updateOrLoad.needtoUpdate || artWrapper.updateOrLoad.needtoLoad)
    downloadArt(artWrapper.art,artWrapper.updateOrLoad.needtoLoad);
  //Проверка на необходимость обновления авторов картины
  QSet<AuthorWrapper>::iterator artAuthorIt=artWrapper.artAuthors.begin();
  for(;artAuthorIt!=artWrapper.artAuthors.end();++artAuthorIt)
  {
    if(artAuthorIt->updateOrLoad.needtoUpdate || artAuthorIt->updateOrLoad.needtoLoad)
    {
      Author artAuthor=artAuthorIt->author;
      downloadAuthor(artAuthor,artWrapper.art.imgName,artWrapper.updateOrLoad.needtoLoad,artAuthorIt->updateOrLoad.needtoLoad);
    }
  }
  artWrapper.artAuthors.clear();
}

void Updater::downloadArt(Art &art, bool needToInsert)
{
  LOADERPIXMAP.load(art.imgPath);
  art.imgPath=LOADERPIXMAP.getPicPath();
  if(needToInsert)
    DB.addArt(art);
  else
    DB.updateArt(art);
  emit fileLoaded();
}

void Updater::downloadAuthor(Author &author, QString artName, bool newArt, bool needToInsert)
{
  LOADERPIXMAP.load(author.imgPath);
  author.imgPath=LOADERPIXMAP.getPicPath();
  if(newArt)
    DB.addArtAuthor(author.authorName,artName);
  if(needToInsert)
  {
    if(!newArt)
      DB.addArtAuthor(author.authorName,artName);
    DB.addAuthor(author);
  }
  else
    DB.updateAuthor(author);
  emit fileLoaded();
}

int Updater::getItemsCountInsideEra(QSet<ArtWrapper>::const_iterator begin, QSet<ArtWrapper>::const_iterator end)
{
  int itemsCount=0;
  for(;begin!=end;++begin)
  {
    if(!begin->updateOrLoad.needtoUpdate && !begin->updateOrLoad.needtoLoad)
      itemsCount+=begin->artAuthors.size();
    else
      itemsCount+=begin->artAuthors.size()+1;
  }
  return itemsCount;

}

void Updater::getErasForUpdateOrLoad(JsonDocument &loadedJson)
{
  QSet<Era> erasFromDB,erasFromLoadedFile;
  //Считали все эпохи из бд и скачанного ДЖСОн
  erasFromDB=DB.getEras();
  erasFromLoadedFile=loadedJson.getEras();

  //итератор на начало списка эпох из джсон
  QSet<Era>::iterator loadedFileIterator=erasFromLoadedFile.begin();

  //обход списка эпох из джсон
  for(;loadedFileIterator!=erasFromLoadedFile.end();++loadedFileIterator)
  {
    UpdateOrLoad updateOrLoad;

    QSet<Era>::iterator iteratorOfFoundedElement=erasFromDB.find(*loadedFileIterator);

    if(iteratorOfFoundedElement!=erasFromDB.end())//нашли эпоху в БД
    {
      if(loadedFileIterator->lastUpdate>iteratorOfFoundedElement->lastUpdate)//Если в скачанном файле новые данные по эпохе
      {
        updateOrLoad.needtoLoad=false;
        updateOrLoad.needtoUpdate=true;
      }
      else //эпоха в джсоне с такой же датой
      {
        //если эпоху и обновлять не надо, то надо внутренности проверить
        updateOrLoad.needtoLoad=false;
        updateOrLoad.needtoUpdate=false;
      }
    }
    else// эпохи нет в бд, значит надо скачать
    {
      updateOrLoad.needtoLoad=true;
      updateOrLoad.needtoUpdate=false;
    }
    EraWrapper eraWrapper(*loadedFileIterator,updateOrLoad);

    QSet<ArtWrapper> domesticArts;
    QSet<ArtWrapper> internationalArts;

    getArtsForUpdateOrLoad(*loadedFileIterator,loadedJson,domesticArts,internationalArts);

    //если картину и эпоху обновлять не надо, а автора надо?

    if(domesticArts.size()>0 || internationalArts.size()>0 ||
       eraWrapper.updateOrLoad.needtoLoad || eraWrapper.updateOrLoad.needtoUpdate)//
    {
      eraWrapper.era=*loadedFileIterator;
      eraWrapper.domesticArtsOfEra=domesticArts;
      eraWrapper.internationalArtsOfEra=internationalArts;
      erasModules.insert(eraWrapper);
    }
  }
}

void Updater::getArtsForUpdateOrLoad(const Era &era, JsonDocument &loadedJson,QSet<ArtWrapper>&domesticArts,
                                     QSet<ArtWrapper>&internationalArts)
{
  QSet<Art> artsFromDB,artsFromLoadedFile;

  //Считали все эпохи из бд и скачанного ДЖСОн
  artsFromDB=DB.getArts(era);
  artsFromLoadedFile=loadedJson.getArts(era);

  QSet<Art>::iterator loadedFileIterator=artsFromLoadedFile.begin();

  for(;loadedFileIterator!=artsFromLoadedFile.end();++loadedFileIterator)
  {
    UpdateOrLoad updateOrLoad;
    QSet<Art>::iterator iteratorOfFoundedElement=artsFromDB.find(*loadedFileIterator);
    if(iteratorOfFoundedElement!=artsFromDB.end())//нашли эпоху в БД
    {
      if(loadedFileIterator->lastUpdate>iteratorOfFoundedElement->lastUpdate)//Если в скачанном файле новые данные по эпохе
      {
        updateOrLoad.needtoLoad=false;
        updateOrLoad.needtoUpdate=true;
      }
      else //эпоха в джсоне с такой же датой
      {
        //если дата такая же, то кол-во авторов в проверке не нуждается
        updateOrLoad.needtoLoad=false;
        updateOrLoad.needtoUpdate=false;
      }
    }
    else// эпохи нет в бд, значит надо скачать
    {
      updateOrLoad.needtoLoad=true;
      updateOrLoad.needtoUpdate=false;
    }

    /*if(!updateOrLoad.needtoLoad && !updateOrLoad.needtoUpdate)
      return;*/

    //если картину надо обновить или загрузить, то
    //Необходимо создать обёртку над ней, заполнить и вернуть либо
    //в отечественное, либо в заруб искусство
    ArtWrapper artWrapper(*loadedFileIterator,updateOrLoad);
    artWrapper.artAuthors = getAuthorsForUpdateOrLoad(*loadedFileIterator,loadedJson);

    if(artWrapper.artAuthors.size()>0 || artWrapper.updateOrLoad.needtoLoad || artWrapper.updateOrLoad.needtoUpdate)
    {
      if(loadedFileIterator->domestic)
        domesticArts.insert(artWrapper);
      else
        internationalArts.insert(artWrapper);
    }
  }
}

QSet<Updater::AuthorWrapper> Updater::getAuthorsForUpdateOrLoad(const Art &art,JsonDocument &loadedJson)
{
  QSet<AuthorWrapper> authorsForUpdate;
  QSet<Author> authorsFromDB,authorsFromLoadedFile;

  //зачем закомментил создание списка всех авторов и сделал вызов функции для каждого

  //authorsFromDB=DB.getAuthorsByNameArt(art.imgName);
  authorsFromLoadedFile=loadedJson.getArtAuthors(art);


  QSet<Author>::iterator loadedFileIterator=authorsFromLoadedFile.begin();

  for(;loadedFileIterator!=authorsFromLoadedFile.end();++loadedFileIterator)
  {
    UpdateOrLoad updateOrLoad;

    //QSet<Author>::iterator iteratorOfFoundedElement=authorsFromDB.find(*loadedFileIterator);

    Author authorForCompare=DB.getAuthorForCompare(loadedFileIterator);
    if(authorForCompare.authorName!="name")//нашли
    {
      if(loadedFileIterator->lastUpdate>authorForCompare.lastUpdate)//Если в скачанном файле новые данные по автору
      {
        updateOrLoad.needtoLoad=false;
        updateOrLoad.needtoUpdate=true;
      }
      else //если в джсон данные не новее
      {
        updateOrLoad.needtoLoad=false;
        updateOrLoad.needtoUpdate=false;
      }
    }
    else //не нашли, значит надо скачать
    {
      updateOrLoad.needtoLoad=true;
      updateOrLoad.needtoUpdate=false;
    }

    //Если allAuthors будет QSet, то при вставке элемента который по алфавиту раньше, он не упорядочивается
    if(std::binary_search(allAuthors.begin(),allAuthors.end(),loadedFileIterator->authorName))
    {
      updateOrLoad.needtoLoad=false;
      updateOrLoad.needtoUpdate=false;
    }
    if(updateOrLoad.needtoUpdate ||  updateOrLoad.needtoLoad)
    {
      AuthorWrapper authorWrapper(*loadedFileIterator,updateOrLoad);
      allAuthors.insert(loadedFileIterator->authorName);
      authorsForUpdate.insert(authorWrapper);
    }
  }
  return authorsForUpdate;
}


