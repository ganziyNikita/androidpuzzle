#ifndef UPDATER_H
#define UPDATER_H

#include <database/levelsdbfacade.h>
#include <database/levelstructures.h>
#include <database/jsondocument.h>
#include <QObject>
#include <vector>
#include <set>
#include <QSet>

class Updater:public QObject
{
  Q_OBJECT
public:
  Updater();
  void UploadSelectedItems(std::vector<EraListModelItem> &selectedEras,int itemsCount);
public slots:
  void getUpdatableItems(const QByteArray&);

private:
  struct UpdateOrLoad
  {
    bool needtoUpdate;
    bool needtoLoad;
  };

  struct AuthorWrapper
  {
    AuthorWrapper(Author author_, UpdateOrLoad updateOrLoad_):
      author(author_),updateOrLoad(updateOrLoad_){
    }

    Author author;
    UpdateOrLoad updateOrLoad;
    bool operator==(const AuthorWrapper &other) const{
      return other.author.authorName==author.authorName;
    }

    friend uint qHash(const AuthorWrapper &authorWrapper){
        return qHash(authorWrapper.author.authorName);
    }
  };

  struct ArtWrapper
  {
    ArtWrapper(Art art_,UpdateOrLoad updateOrLoad_):
      art(art_),updateOrLoad(updateOrLoad_){
    }
    Art art;
    UpdateOrLoad updateOrLoad;
    QSet<AuthorWrapper> artAuthors;

    friend uint qHash(const ArtWrapper &artWrapper){
        return qHash(artWrapper.art.imgName);
    }

    bool operator==(const ArtWrapper &other) const{
      return other.art.imgName==art.imgName;
    }
  };

  struct EraWrapper
  {

    EraWrapper(Era era_,UpdateOrLoad updateOrLoad_):
      era(era_),updateOrLoad(updateOrLoad_){
    }
    Era era;
    mutable UpdateOrLoad updateOrLoad;
    mutable QSet<ArtWrapper> domesticArtsOfEra;
    mutable QSet<ArtWrapper> internationalArtsOfEra;

    /*friend uint qHash(const EraWrapper &eraWrapper){
        return qHash(eraWrapper.era.name);
    }*/

    friend uint qHash(const EraWrapper &other){
        return qHash(other.era.name);
    }

    //bool operator<(const QString& other) { return other<era.name;}
    //bool operator>(const QString& other) { return other<era.name;}

    bool operator==(const EraWrapper &other) const{
      return other.era.name==era.name;
    }

  };

  void downloadEraModule(const EraWrapper &eraWrapper, bool updateDomesticFiles, bool updateInternationalFiles);
  void downloadEra(const Era &era,bool needToInsert);
  void downloadArtModule(ArtWrapper &artWrapper);
  void downloadArt(Art &art,bool needToInsert);
  void downloadAuthor(Author &author,QString artName,bool newArt, bool needToInsert);


  int getItemsCountInsideEra(QSet<Updater::ArtWrapper>::const_iterator begin,
                             QSet<Updater::ArtWrapper>::const_iterator end);

  void getErasForUpdateOrLoad(JsonDocument &loadedJson);
  void getArtsForUpdateOrLoad(const Era &era,JsonDocument &loadedJson,
                              QSet<ArtWrapper>&domesticArts, QSet<ArtWrapper>&internationalArts);
  QSet<AuthorWrapper> getAuthorsForUpdateOrLoad(const Art &art,JsonDocument &loadedJson);

  QSet<EraWrapper> erasModules;
  std::set<QString> allAuthors;
signals:
  void itemsLoaded(std::vector<EraListModelItem> vec, bool isTestingModule);
  void maxValueCalculated(int maxValue);
  void fileLoaded();
};

#define UPDATER Singleton<Updater>::instance()

#endif // UPDATER_H
