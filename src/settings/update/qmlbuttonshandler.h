#ifndef QMLBUTTONSHANDLER_H
#define QMLBUTTONSHANDLER_H
#include <QObject>

class QmlButtonsHandler: public QObject
{
  Q_OBJECT
public:
  //Функции, вызываемые из QML
  Q_INVOKABLE void backButtonPressed()
  {
    emit back();
  }
  Q_INVOKABLE void loadButtonPressed()
  {
    emit load();
  }
  Q_INVOKABLE void whichLevelIsSelected(int buttonNumber)
  {
    emit selectedLevel(buttonNumber);
  }
signals:
  void load();
  void back();
  void selectedLevel(int buttonNumber);
  void eraQuestionsCount(int count);
  void artQuestionsCount(int count);
};


#endif // QMLBUTTONSHANDLER_H
