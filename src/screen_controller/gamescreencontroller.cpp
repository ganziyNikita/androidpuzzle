#include "gamescreencontroller.h"
#include "game/puzzlegame.h"
#include "html_view/helpviewer.h"
#include "html_view/artinfowidget.h"
#include "database/levelsdbfacade.h"
#include "database/levelstructures.h"
#include <memory>
#include <vector>

GameScreenController::GameScreenController(QWidget* parent)
  : ScreensStack(parent) {

  m_game = new PuzzleGame(this);
  m_artInfo = new ArtInfoWidget(this);

  m_game->hide();
  m_artInfo->hide();

  connect(m_game, &PuzzleGame::back, [=] { pop(); });
  connect(m_artInfo, &ArtInfoWidget::back, [=] { pop(); pop(); });

  connect(m_game, &PuzzleGame::finished, [=] {
    std::vector<Author> authors = DB.authorsByNameArt(m_currentArt->imgName);
    m_artInfo->load(*m_currentArt, authors);
    push(m_artInfo);
  });
}

void GameScreenController::startRandomGame(Mode mode) {
  start(DB.randomArt(), mode);
}

void GameScreenController::startRandomGame(Author author, Mode mode) {
  start(DB.randomArt(author), mode);
}

void GameScreenController::startRandomGame(Era era, Mode mode) {
  start(DB.randomArt(era), mode);
}

void GameScreenController::start(Art art, Mode mode) {
  m_currentArt = make_unique<Art>(art);
  QPixmap pixmap(m_currentArt->imgPath);

  m_game->setLabel(m_currentArt->imgPath);
  m_game->load(pixmap, mode);
  push(m_game);
}

