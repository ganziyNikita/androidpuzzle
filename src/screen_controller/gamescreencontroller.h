#ifndef GAMESCREENCONTROLLER_H
#define GAMESCREENCONTROLLER_H

#include "screensstack.h"
#include "mode.h"
#include "database/levelstructures.h"
#include <memory>

class PuzzleGame;
class HelpViewer;
class ArtInfoWidget;

class GameScreenController : public ScreensStack {
  Q_OBJECT
public:
  explicit GameScreenController(QWidget *parent = nullptr);
  void startRandomGame(Mode mode);
  void startRandomGame(Author author, Mode mode);
  void startRandomGame(Era era, Mode mode);
  void start(Art art, Mode mode);
protected:
   PuzzleGame *m_game;
   ArtInfoWidget *m_artInfo;

   std::unique_ptr<Art> m_currentArt;
};

#endif // GAMESCREENCONTROLLER_H
