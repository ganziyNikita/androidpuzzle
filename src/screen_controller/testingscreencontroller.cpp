#include "testingscreencontroller.h"
#include "testing/testmanager.h"
#include "settings/update/eralistmodel.h"
#include "testing/models/draggridmodel.h"
#include "testing/models/dropgridmodel.h"
#include <QQmlEngine>

TestingScreenController::TestingScreenController(QWidget *parent)
  :ScreensStack(parent)
{
  qmlRegisterType<EraListModel>("EraListModel",1,0,"EraListModel");
  qmlRegisterType<DropGridModel>("dropgridmodel",1,0,"DropGridModel");
  qmlRegisterType<DragGridModel>("draggridmodel",1,0,"DragGridModel");

  m_testing=new TestSettingsWidget(this);
  m_testWindow=new WidgetOfTesting(this);
  //push(m_testing);
  //TESTMANAGER.loadAvailableEras();

  connect(m_testing,&TestSettingsWidget::backButtonPressed, [=] {emit back(); pop();});
  //Начало тестирования
  connect(m_testing,&TestSettingsWidget::startTestingButtonPressed, [=]{
    push(m_testWindow);
  });
}

void TestingScreenController::pushTestingWindow()
{
  push(m_testing);
  TESTMANAGER.loadAvailableEras();
}

