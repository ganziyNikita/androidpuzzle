#include "testsettingswidget.h"
#include "testmanager.h"


TestSettingsWidget::TestSettingsWidget(QWidget *parent):QQuickWidget(parent)
{
  setStyleSheet("background-color: 'grey';");
  this->rootContext()->setContextProperty("buttonsHandler",buttonsHandler);
  qDebug()<<"BUTTONSHANDLER ADDED";
  this->setSource(QUrl(QStringLiteral("qrc:/qmlWindows/DragAndDropWindow/TestSettingsWindow.qml")));
  this->setResizeMode(QQuickWidget::SizeRootObjectToView);
  this->setAttribute(Qt::WA_AlwaysStackOnTop);
  this->setClearColor(Qt::transparent);

  //Delete signal whichLevelIsSelected
  /*connect(buttonsHandler,&QmlButtonsHandler::whichLevelIsSelected, [=] (int buttonNumber){
    TESTMANAGER.startTesting(buttonNumber);
  });*/
  connect(buttonsHandler,&QmlButtonsHandler::back,[=] {emit backButtonPressed();});
  connect(buttonsHandler,&QmlButtonsHandler::load,[=]{emit startTestingButtonPressed();});

  connect(&TESTMANAGER,&TestManager::eraQuestionsCountReady,[=](int count){
    emit buttonsHandler->eraQuestionsCount(6);
  });

  connect(&TESTMANAGER,&TestManager::artQuestionsCountReady,[=](int count){
    emit buttonsHandler->artQuestionsCount(6);
  });
}

