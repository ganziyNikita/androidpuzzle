#include "testmanager.h"
#include "database/levelsdbfacade.h"
#include "database/levelstructures.h"
#include <random>
#include <time.h>


TestManager::TestManager()
{
  srand((int)time(NULL));

}

int TestManager::convertButtonNumberToNumberQuestions(int buttonNumber)
{
  int countOfQuestions=0;

  if(buttonNumber==0)
    countOfQuestions=5*4;
  else if(buttonNumber==1)
    countOfQuestions=10*4;
  else
    countOfQuestions=15*4;
  return countOfQuestions;
}

void TestManager::loadAvailableEras()
{
  //Обход каждой эпохи
  //для каждой эпохи найти количество картин
  //для каждой картины найти авторов и закинуть в общий список без повторений
  //использовать сочетания эпоха+автор, автор+картина, эпоха+картина
  //во время обхода записывать эти комбинации в мэп

  //Для каждой эпохи найти картины
  //для каждой картины найти авторов O(n^3)

  ///Эпохи не различаются по признаку отечественное и зарубежное
  /// Нужно смотреть картины

  std::vector<EraListModelItem> availableForTestingEras;
  std::vector<Era> allEras;

  allEras=DB.eras();
  std::vector<Era>::iterator erasIt=allEras.begin();

  for(;erasIt!=allEras.end();erasIt++)
  {
    std::vector<Art> eraArts=DB.arts(*erasIt);
    std::vector<Art>::iterator artsIt=eraArts.begin();
    if(eraArts.size()>0)
    {
      bool hasDomesticArts=false;
      bool hasInternationalArts=false;

      std::vector<eraQuestionsWrapper> eraQuestionsAndAnswers;
      std::vector<authorQuestionsWrapper> authorQuestionsAndAnswers;
      for(;artsIt!=eraArts.end();artsIt++)
      {
        //Добавление вопроса Картина+эпоха
        eraQuestionsAndAnswers.push_back(eraQuestionsWrapper(*artsIt,*erasIt));
        std::vector<Author> artAuthors=DB.getAuthorsOfArt(artsIt->imgName);

        if(artsIt->domestic && !hasDomesticArts)
          hasDomesticArts=true;
        else if(!artsIt->domestic && !hasInternationalArts)
          hasInternationalArts=true;

        std::vector<Author>::iterator authorsIt=artAuthors.begin();

        for(;authorsIt!=artAuthors.end();authorsIt++)
        {
          //Добавление вопроса Картина+автор
          authorQuestionsAndAnswers.push_back(authorQuestionsWrapper(*artsIt,*authorsIt));
        }
      }
      //У эпохи, при их наличии, указывается любое ненулевое количество отечественных и заруб файлов.
      availableForTestingEras.push_back(EraListModelItem(erasIt->name,2,2,false,
                                                         false,hasDomesticArts,hasInternationalArts));
      eraModule.push_back(eraModuleQuestionsWrapper(*erasIt,eraQuestionsAndAnswers,authorQuestionsAndAnswers));
    }
  }

  emit erasReady(availableForTestingEras,true);
  checkNumberOfQuestions();
}

void TestManager::getNumberOfQuestions(int eraQuestions, int artQuestions){
  std::vector<eraModuleQuestionsWrapper>::iterator it=eraModule.begin();
  //подсчёт количества составленных вопросов, чтобы отключить возможность выбора большего количества
  //вопросов для теста
  for(;it!=eraModule.end();it++)
  {
    eraQuestions+=it->eraQuestionsAndAnswers.size();
    artQuestions+=it->authorQuestionsAndAnswers.size();
  }
}

void TestManager::checkNumberOfQuestions()
{
  int erasQuestionsCount=0, artsQuestionsCount=0;
  getNumberOfQuestions(erasQuestionsCount,artsQuestionsCount);

  //Сигнал о кол-ве вопросов принимает окно настроек тестирования
  //Исходя из кол-ва отключает возможность выбора большего кол-ва вопросов
  emit eraQuestionsCountReady(erasQuestionsCount/4);
  emit artQuestionsCountReady(artsQuestionsCount/4);
}

void TestManager::startTesting(std::vector<EraListModelItem> selectedEras, int buttonNumber)
{

  ///псевдо-генератор для std::shuffle
  auto rng = std::default_random_engine {};

  int eraQuestions=0, artQuestions=0,countOfQuestions=0;
  //Какое количество вопросов позволяет создать бд
  //Отключить кнопку начала, если вопросов недостаточно todo
  getNumberOfQuestions(eraQuestions,artQuestions);

  //Какое количество вопросов будет в тесте
  countOfQuestions=convertButtonNumberToNumberQuestions(buttonNumber);

  std::vector<eraQuestionsWrapper> eraQuestionsQuad_;
  std::vector<authorQuestionsWrapper> authorQuestionsQuad_;

  for(int index=0;countOfQuestions!=0;index++)
  {
    if(index>=int(selectedEras.size()))
      index=0;
    std::vector<eraModuleQuestionsWrapper>::iterator foundElemIt=
        std::find(eraModule.begin(), eraModule.end(), selectedEras[index].eraName);
    if(foundElemIt != eraModule.end())
    {
      int eraQuestionsSize=foundElemIt->eraQuestionsAndAnswers.size();
      int authorQuestionsSize=foundElemIt->authorQuestionsAndAnswers.size();

      if(eraQuestionsSize==0 && authorQuestionsSize==0)
      {
        ///удаление пустого модуля вопросов
        foundElemIt=eraModule.erase(foundElemIt);
      }

      //Обеспечить наличие каждой из выбранных эпох?
      if(eraQuestionsSize!=0)
      {
        std::shuffle(foundElemIt->eraQuestionsAndAnswers.begin(),
                     foundElemIt->eraQuestionsAndAnswers.end(),rng);
        eraQuestionsQuad_.push_back(*foundElemIt->eraQuestionsAndAnswers.begin());
        //удаление взятого элемента
        foundElemIt->eraQuestionsAndAnswers.begin()=foundElemIt->eraQuestionsAndAnswers.erase
            (foundElemIt->eraQuestionsAndAnswers.begin());

        //собрали блок вопросов и передали в общий список вопросов
        if(eraQuestionsQuad_.size()==4)
        {
          eraQuadQuestions=eraQuestionsQuad_;
          eraQuestionsQuad_.clear();
          countOfQuestions-=4;
        }
      }

      if(authorQuestionsSize!=0)
      {
        std::shuffle(foundElemIt->authorQuestionsAndAnswers.begin(),
                     foundElemIt->authorQuestionsAndAnswers.end(),rng);

        authorQuestionsQuad_.push_back(*foundElemIt->authorQuestionsAndAnswers.begin());
        //удаление взятого элемента
        foundElemIt->authorQuestionsAndAnswers.begin()=foundElemIt->authorQuestionsAndAnswers.erase
            (foundElemIt->authorQuestionsAndAnswers.begin());

        //собрали блок вопросов и передали в общий список вопросов
        if(authorQuestionsQuad_.size()==4 && countOfQuestions!=0)
        {
          authorQuadQuestions=authorQuestionsQuad_;
          authorQuestionsQuad_.clear();
          countOfQuestions-=4;
        }
      }
    }
  }

  //перемешать вопросы, чтобы ответ с вопросом не находились в угадываемых позициях
  std::shuffle(eraQuadQuestions.begin(),eraQuadQuestions.end(),rng);
  std::shuffle(authorQuadQuestions.begin(),authorQuadQuestions.end(),rng);
  sendQuadToDndModels();
}

void TestManager::takeResultsFromDropModel(std::vector<DropGridItem> results)
{
  //Обработать результаты

  //maybe хранить пару вопрос+ответ в мэпе изначально и показывать юзеру кол-во уникальных вопросов
  //тогда каждый раз для эпохи будут одинаковые вопросы
  int residueCount=4;
  findCorrectAnswers(results,residueCount);
  findWrongAnswers(results,residueCount);
  sendQuadToDndModels();
}

void TestManager::findCorrectAnswers(std::vector<DropGridItem> &results, int &residueCount)
{
  std::vector<DropGridItem>::iterator dropIt=results.begin();
  //если вопрос был по эпохам
  if(currentTypeOfQuestionIsEra)
  {
    for(;dropIt!=results.end();dropIt++)
    {
      std::vector<eraQuestionsWrapper>::iterator eraQuestionsIt=eraQuadQuestions.begin();
      for(;eraQuestionsIt!=eraQuadQuestions.end();eraQuestionsIt++)
      {
        //если найдена пара вопрос+ответ как дал пользователь
        if((dropIt->answerObjectName==eraQuestionsIt->art.imgName)
           &&dropIt->dropItemName==eraQuestionsIt->era.name)
        {
          dropIt=results.erase(dropIt--);
          residueCount--;
          //удалили вопрос из общего списка
          eraQuestionsIt=eraQuadQuestions.erase(eraQuestionsIt);
          break;
        }
        //иначе ищем дальше
      }
    }
  }
  //если вопрос был по авторам
  else
  {
    for(;dropIt!=results.end();dropIt++)
    {
      std::vector<authorQuestionsWrapper>::iterator authorQuestionsIt=authorQuadQuestions.begin();
      for(;authorQuestionsIt!=authorQuadQuestions.end();authorQuestionsIt++)
      {
        //если найдена пара вопрос+ответ как дал пользователь
        if((dropIt->answerObjectName==authorQuestionsIt->art.imgName)
           &&dropIt->dropItemName==authorQuestionsIt->author.authorName)
        {
          dropIt=results.erase(dropIt--);
          residueCount--;
          //удалили вопрос из общего списка
          authorQuestionsIt=authorQuadQuestions.erase(authorQuestionsIt);
          break;
        }
        //иначе ищем дальше
      }
    }
  }

  //заполнять здесь массив результатов? todo
}

//quadResidue остаток из четвёрки вопросов, который ещё не обработан (удалён т.к. дан корректный ответ)
void TestManager::findWrongAnswers(std::vector<DropGridItem> &results, int &residueCount)
{
  std::vector<DropGridItem>::iterator dropIt=results.begin();
  //если вопрос был по эпохам
  if(currentTypeOfQuestionIsEra)
  {
    std::vector<eraQuestionsWrapper>::iterator AQit=eraQuadQuestions.begin();
    for(;dropIt!=results.end();dropIt++)
    {
      std::vector<eraQuestionsWrapper>::iterator eraQuestionsIt=eraQuadQuestions.begin();
      for(;eraQuestionsIt!=eraQuadQuestions.end();eraQuestionsIt++)
      {
        //вопрос пользователя найден
        if(dropIt->answerObjectName==eraQuestionsIt->art.imgName)
        {
          //заполнение массива корректным ответом
          dropIt=results.erase(dropIt--);
          residueCount--;
          //удалили вопрос из общего списка
          eraQuestionsIt=eraQuadQuestions.erase(eraQuestionsIt);
          break;
        }
      }
    }
  }
  //если вопрос был по авторам
  else
  {
    for(;dropIt!=results.end();dropIt++)
    {
      std::vector<authorQuestionsWrapper>::iterator authorQuestionsIt=authorQuadQuestions.begin();
      for(;authorQuestionsIt!=authorQuadQuestions.end();authorQuestionsIt++)
      {
        //если ответ пользователя верен
        if(dropIt->answerObjectName==authorQuestionsIt->art.imgName)
        {
          dropIt=results.erase(dropIt--);
          residueCount--;

          //удалили вопрос из общего списка
          //надо ли присваивать итератору результат? всё равно брейк
          authorQuestionsIt=authorQuadQuestions.erase(authorQuestionsIt);
          break;
        }
      }
    }
  }
}

void TestManager::sendQuadToDndModels()
{
  //если вопросы закончились
  if(eraQuadQuestions.size()==0)
  {
    emit questionsIsOver();
    return;
  }

  auto rng = std::default_random_engine {};

  std::vector<DragGridItem> dragGridItems;
  std::vector<DropGridItem> dropGridItems;

  //рандомный выбор типа вопроса
  int questionType=getRandomValueInRange(0,1);

  qDebug()<<"ERAS COUNT"+QString::number(eraQuadQuestions.size());
  qDebug()<<"AUTHORS COUNT"+QString::number(authorQuadQuestions.size());

  for(int index=0;index<4;index++)
  {
    //вопросы картина+эпоха
    if(questionType && eraQuadQuestions.size()<4)
    {
      addDragItem(eraQuadQuestions[index].art,dragGridItems);
      addDropItem(eraQuadQuestions[index].era,dropGridItems);
      currentTypeOfQuestionIsEra=true;
    }
    else //вопросы картина+автор
    {
      addDragItem(authorQuadQuestions[index].art,dragGridItems);
      addDropItem(authorQuadQuestions[index].author,dropGridItems);
      currentTypeOfQuestionIsEra=false;
    }
  }


  //перемешать четвёрку вопросов
  std::shuffle(dragGridItems.begin(),dragGridItems.end(),rng);
  std::shuffle(dropGridItems.begin(),dropGridItems.end(),rng);

  dragModelListReady(dragGridItems);
  dropModelListReady(dropGridItems);

}


///Удаление первых двух символов вызвано особенностью подхода qml к файловым путям
/// Путь к любой картинке начинается со :/pictures_db
/// qml склеивает путь картинки с путём к директории с самим qml файлом
/// Подъём до верхней категории("../../") остановится на qrc:/

void TestManager::addDragItem(Art art, std::vector<DragGridItem> &dragItems)
{
  DragGridItem dragItem("Picture",
                        art.imgName,
                        art.imgPath.remove(0,2),
                        -1);
  dragItems.push_back(dragItem);
}

void TestManager::addDropItem(Era era, std::vector<DropGridItem> &dropItems)
{
  qDebug()<<"ERA PATH"+era.imgPath;
  DropGridItem dropItem("Picture",
                        era.name,
                        era.imgPath.remove(0,2),
                        "");
  dropItems.push_back(dropItem);
}

void TestManager::addDropItem(Author author, std::vector<DropGridItem> &dropItems)
{
  qDebug()<<"ERA PATH"+author.imgPath;
  DropGridItem dropItem("Picture",
                        author.authorName,
                        author.imgPath.remove(0,2),
                        "");
  dropItems.push_back(dropItem);
}

int TestManager::getRandomValueInRange(int min, int max)
{
  return ((rand()%max)+min);
}
