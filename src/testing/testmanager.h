#ifndef TESTMANAGER_H
#define TESTMANAGER_H

#include <QSet>
#include <QMap>
#include "database/levelstructures.h"
#include "singleton.h"

class TestManager:public QObject
{
  Q_OBJECT
public:
  TestManager();
  int convertButtonNumberToNumberQuestions(int buttonNumber);
  void loadAvailableEras();
  void checkNumberOfQuestions();
  void getNumberOfQuestions(int eraQuestions, int artQuestions);
  void startTesting( std::vector<EraListModelItem>, int requiredCountOfQuestions);
  void takeResultsFromDropModel(std::vector<DropGridItem> results);
signals:
  void erasReady(std::vector<EraListModelItem> vec, bool isTestingModule);
  void eraQuestionReady();
  void artQuestionReady();
  void eraQuestionsCountReady(int count);
  void artQuestionsCountReady(int count);
  void dragModelListReady(std::vector<DragGridItem>);
  void dropModelListReady(std::vector<DropGridItem>);
  void questionsIsOver();
private:

  void findCorrectAnswers(std::vector<DropGridItem> &results, int &quadResidue);
  void findWrongAnswers(std::vector<DropGridItem> &results, int &quadResidue);
  void sendQuadToDndModels();

  void addDragItem(Art art, std::vector<DragGridItem> &dragItems);
  void addDropItem(Era era, std::vector<DropGridItem> &dropItems);
  void addDropItem(Author author, std::vector<DropGridItem> &dropItems);

  int getRandomValueInRange(int min, int max);

  struct eraQuestionsWrapper{
    Art art;
    Era era;
    eraQuestionsWrapper(Art art_, Era era_):
      art(art_),era(era_){}
  };

  struct authorQuestionsWrapper{
    Art art;
    Author author;
    authorQuestionsWrapper(Art art_, Author author_):
      art(art_),author(author_){}
  };

  //нормальное название придумать

  struct eraModuleQuestionsWrapper{
    Era era;

    std::vector<eraQuestionsWrapper> eraQuestionsAndAnswers;
    std::vector<authorQuestionsWrapper> authorQuestionsAndAnswers;

    eraModuleQuestionsWrapper(Era era_,std::vector<eraQuestionsWrapper> eraQuestionsAndAnswers_,
                        std::vector<authorQuestionsWrapper> authorQuestionsAndAnswers_):
      era(era_), eraQuestionsAndAnswers(eraQuestionsAndAnswers_),
      authorQuestionsAndAnswers(authorQuestionsAndAnswers_){}

    /*friend uint qHash(const eraModuleQuestionsWrapper &other){
      return qHash(other.era.name);
    }*/

    bool operator==(const QString &findEraName) const {
      return era.name==findEraName;
    }

  };


  std::vector<eraModuleQuestionsWrapper> eraModule;
  std::vector<eraQuestionsWrapper> eraQuadQuestions;
  std::vector<authorQuestionsWrapper> authorQuadQuestions;

  bool currentTypeOfQuestionIsEra=false;
};


#define TESTMANAGER Singleton<TestManager>::instance()

#endif // TESTMANAGER_H
