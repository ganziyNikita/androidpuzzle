#include "iconsview.h"
#include "database/levelsdbfacade.h"

#include <QVBoxLayout>
#include <QScrollBar>
#include <QResizeEvent>
#include <QGraphicsProxyWidget>

IconsView::IconsView(QWidget *parent)
  : QGraphicsView(parent), m_scene(this) {
  setInteractive(false);
  setDragMode(QGraphicsView::DragMode::ScrollHandDrag);

  setViewportMargins(0, 0, 0, 0);

  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

  setScene(&m_scene);
}

void IconsView::disposeIcons() {
  const int n = m_icons.size();

  const int NRow = n/NColumn + (n%NColumn?1:0);

  int i = 0;
  for (auto icon : m_icons) {
    int iRow = i/NColumn;
    int iColumn = i%NColumn;

    if (height() > width()) {
      std::swap(iRow, iColumn);
    }

    icon->move(iRow*IconSize, iColumn*IconSize);
    icon->setLabelSize(IconSize, IconSize);

    ++i;
  }

  if (height() > width()) {
    m_scene.setSceneRect(0, 0, IconSize*NColumn, IconSize*NRow);
  }
  else {
    m_scene.setSceneRect(0, 0, IconSize*NRow, IconSize*NColumn);
  }
  fitInView(0, 0, IconSize*NColumn, IconSize*NColumn, Qt::KeepAspectRatio);
}

void IconsView::resizeEvent(QResizeEvent *event) {
  if (event->size() != event->oldSize())
    disposeIcons();
}

void IconsView::mousePressEvent(QMouseEvent *event) {
  m_clickPos = event->pos();
  QGraphicsView::mousePressEvent(event);
}

void IconsView::mouseReleaseEvent(QMouseEvent *event) {
  auto object = itemAt(event->pos());
  do {
    if (nullptr == object)
      break;

    if ((m_clickPos - event->pos()).manhattanLength() > 10)
      break;

    auto proxy = dynamic_cast<QGraphicsProxyWidget*>(object);
    if (nullptr == proxy)
      break;

    auto authorIcon = dynamic_cast<AuthorIcon*>(proxy->widget());
    auto eraIcon = dynamic_cast<EraIcon*>(proxy->widget());
    auto artIcon = dynamic_cast<ArtIcon*>(proxy->widget());

    if (authorIcon) {
      emit authorSelected(authorIcon->getAuthor());
    }
    if (eraIcon) {
      emit eraSelected(eraIcon->getEra());
    }
    if (artIcon) {
      emit artSelected(artIcon->getArt());
    }
  } while(false);

  QGraphicsView::mouseReleaseEvent(event);
}

void IconsView::loadAuthors() {
  m_scene.clear();
  m_icons.clear();

  for (auto author : DB.authors()) {
    AuthorIcon *icon = new AuthorIcon(std::move(author));
    m_icons[author.imgPath] = icon;

    m_scene.addWidget(icon);
    icon->show();
  }
  disposeIcons();
}

void IconsView::loadEras() {
  m_scene.clear();
  m_icons.clear();

  for (auto era : DB.eras()) {
    EraIcon *icon = new EraIcon(std::move(era));

    m_icons[era.imgPath] = icon;

    m_scene.addWidget(icon);
    icon->show();
  }

  disposeIcons();
}

void IconsView::loadArts(Author author) {
  m_scene.clear();
  m_icons.clear();

  for (auto art : DB.arts(author)) {
    ArtIcon *icon = new ArtIcon(std::move(art));

    m_icons[art.imgPath] = icon;

    m_scene.addWidget(icon);
    icon->show();
  }

  disposeIcons();
}

void IconsView::loadArts(Era era) {
  m_scene.clear();
  m_icons.clear();

  for (auto art : DB.arts(era)) {
    ArtIcon *icon = new ArtIcon(std::move(art));

    m_icons[art.imgPath] = icon;

    m_scene.addWidget(icon);
    icon->show();
  }

  disposeIcons();
}



