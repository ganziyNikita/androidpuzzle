#ifndef LEVELICON_H
#define LEVELICON_H

#include <QLabel>
#include "database/levelstructures.h"

class ILevelIcon : public QLabel {
  Q_OBJECT
public:
  explicit ILevelIcon(QWidget *parent = nullptr);
  virtual void setLabelSize(const int w, const int h) = 0;
  virtual ~ILevelIcon() = default;
};

class LevelGraphicsIcon : public ILevelIcon {
  Q_OBJECT
public:
  explicit LevelGraphicsIcon(QWidget *parent = nullptr);
  virtual void setLabelSize(const int w, const int h);
protected:
  virtual QPixmap iconImage() = 0;
};

class AuthorIcon : public LevelGraphicsIcon {
  Q_OBJECT
public:
  explicit AuthorIcon(Author&& author_, QWidget *parent = nullptr);
  Author getAuthor();
protected:
  virtual QPixmap iconImage();

  Author author;
};

class EraIcon : public LevelGraphicsIcon {
  Q_OBJECT
public:
  explicit EraIcon(Era&& era_, QWidget *parent = nullptr);
  Era getEra();
protected:
  virtual QPixmap iconImage();

  Era era;
};

class ArtIcon : public LevelGraphicsIcon {
  Q_OBJECT
public:
  explicit ArtIcon(Art&& art_, QWidget *parent = nullptr);
  Art getArt();
protected:
  virtual QPixmap iconImage();

  Art  art;
};

#endif // LEVELICON_H
