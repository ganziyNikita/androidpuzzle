#ifndef ICONSVIEW_H
#define ICONSVIEW_H

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMap>
#include "menu/level_icons_view/levelicon.h"

class IconsView : public QGraphicsView {
  Q_OBJECT
public:
  IconsView(QWidget *parent = nullptr);

  void loadAuthors();
  void loadEras();
  void loadArts(Author author);
  void loadArts(Era era);
protected:
  void resizeEvent(QResizeEvent *event) override;
  void disposeIcons();

  void mousePressEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent *event) override;
signals:
  void authorSelected(Author data);
  void eraSelected(Era data);
  void artSelected(Art data);
protected:
  QGraphicsScene m_scene;
  QPoint m_clickPos;
  QMap<QString, LevelGraphicsIcon*> m_icons;

  const int IconSize = 150;
  const int NColumn = 2;
};

#endif // ICONSVIEW_H
